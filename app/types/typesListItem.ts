import {ICompany, IDepartment, IEmployee} from '../types/typesEntities';

export interface IListItemDispatchProps {
    onEditButtonClick?: () => void;
    onDeleteButtonClicked?: () => void;
}

export interface IListItemOwnProps {
    order: number;
    key: string;
}

export interface IDepartmentStateProps {
    companyTitle: string;
}

export interface IEmployeeStateProps {
    companyTitle: string;
    departmentTitle: string;
}

export interface ICompanyListItemProps extends ICompany, IListItemDispatchProps, IListItemOwnProps {}

export interface IEmployeeListItemProps extends IEmployee, IListItemDispatchProps, IListItemOwnProps, IEmployeeStateProps {}

export interface IDepartmentListItemProps extends IDepartment, IListItemDispatchProps, IListItemOwnProps, IDepartmentStateProps {}
