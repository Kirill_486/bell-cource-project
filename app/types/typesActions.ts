import { Action } from 'redux';
import {Entity} from './typesEntities';

export interface IActionType extends Action {
  payload: any;
}

export interface IEntitySingleAction<E extends Entity> extends Action {
  payload: E
}

export interface IEntityArrayAction<E extends Entity> extends Action {
  payload: E[]
}

export type IEntityAction<E extends Entity> = IEntitySingleAction<E> | IEntityArrayAction<E>;
