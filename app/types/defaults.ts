import {ICompany, IDepartment, IEmployee} from './typesEntities';

export const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export const companyDefaults: ICompany = {
  id:'',
  title: 'none',
  address: 'nowhere',
  inn: 'nulll'
}

export const departmentDefaults: IDepartment = {
  id: '',
  companyId: '',
  phone: '111111',
  title: 'Lazyness dept.'
}
export const employeeDefaults: IEmployee = {
  id: '',
  companyId: '',
  departmentId: '',
  address: 'Nowhere',
  fullName: 'John Doe',
  position: 'candidate'
}
