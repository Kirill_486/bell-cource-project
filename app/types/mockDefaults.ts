import {ICredentials} from './typesEntities'

// Some defaults that will be used in mocking backend

export const users: ICredentials[] = [
  {
    email:'lambda.function.486@gmail.com',
    password: 'admin111'
  },
  {
    email: 'user@test.ru',
    password: 'test'
  }
];
