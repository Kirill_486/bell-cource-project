import {Entity, ICompany, IDepartment, IEmployee} from './typesEntities';

export interface ICompanyFormState extends ICompany {
    errorMessage: string;
}

export interface IDepartmentFormState extends IDepartment {
    errorMessage: string;
}

export interface IEmployeeFormState extends IDepartment {
    errorMessage: string;
}

export interface IFormDispatchProps<E extends Entity> {
    closeModal: () => void;
    saveEntity: (entity: E) => void;
}

export interface IDepartmentFormProps extends IDepartment, IFormDispatchProps<IDepartment> {
    isNew: boolean;
}

export interface ICompanyFormProps extends ICompany, IFormDispatchProps<ICompany> {
    isNew: boolean;
}

export interface IEmployeeFormProps extends IEmployee, IFormDispatchProps<IEmployee> {
    isNew: boolean;
}
