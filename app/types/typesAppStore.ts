import {ICompany, IDepartment, IEmployee} from './typesEntities';
import {IModalState} from './typesModal';

export interface IApplicationState {
    companies: ICompany[];
    departments: IDepartment[];
    employees: IEmployee[];
    auth: IAuthState;
    companyModal: IModalState<ICompany>;
    departmentModal: IModalState<IDepartment>;
    employeeModal: IModalState<IEmployee>;
}

export interface IAuthState {
  data?: number;
  loginStatus: boolean;
  loading: boolean;
  userName: string;
}
