import { RouteComponentProps } from 'react-router';
import {Entity} from './typesEntities';

export interface IDashboardParams {
  companyId?: string;
  departmentId?: string;
}

export interface IDashboardStateProps<E extends Entity> {
  data: E[];
};

export interface IDashboardDispatchProps {
  openCreateModal?: ()=> void;
  fetchData?: () => void;
}

export interface IDashboardProps<E extends Entity> extends RouteComponentProps<IDashboardParams>, IDashboardStateProps<E>, IDashboardDispatchProps {
}
