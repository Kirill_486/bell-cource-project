export interface ICompany {
    id: string;
    title: string;
    address: string;
    inn: string;
};

export interface IDepartment {
  id?: string;
  companyId: string;
  title: string;
  phone: string;
}

export interface IEmployee {
  id?: string;
  departmentId: string;
  companyId: string;
  fullName: string;
  address: string;
  position: string;
}

export type Entity = ICompany | IDepartment | IEmployee;

export interface ICredentials {
  email: string;
  password: string;
}
