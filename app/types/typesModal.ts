import {Entity} from './typesEntities';

export interface IModalState<E extends Entity> extends IModalStateProps<E> {
  isModalOpen: boolean;
  isNew: boolean;
}

export interface IModalStateProps<E extends Entity> {
  entity: E;
}

export interface IModalDispatchProps {
  closeModal?: () => void;
}

export interface IModalProps<E extends Entity> extends IModalState<E>, IModalDispatchProps, IModalStateProps<E> {}
