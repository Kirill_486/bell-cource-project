export enum ActionTypes {
  LOGIN = 'ACTION_LOGIN',
  LOGOUT = 'ACTION_LOGOUT',
  CLICK = 'ACTION_CLICK',
}
export enum AsyncActionTypes {
  BEGIN = '_BEGIN',
  SUCCESS = '_SUCCESS',
  FAILURE = '_FAILURE',
}
export enum Entities {
  COMPANY = '_COMPANY',
  DEPARTMENT = '_DEPARTMENT',
  PERSON = '_PERSON'
}
export enum CRUD {
  CREATE = 'CREATE',
  READ   = 'READ',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE'
}
export enum ModalActionType {
  OPENFORCREATE = 'OPENFORCREATE',
  OPENFOREDIT = 'OPENFOREDIT',
  CLOSE = 'CLOSE'
}
