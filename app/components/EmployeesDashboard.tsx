import * as React from 'react';
import { connect } from 'react-redux';

import { ThunkDispatch } from 'redux-thunk';
import {asyncReadEmployees} from '../store/actions/employeeActions';
import {openEmployeeModalForCreate} from '../store/actions/employeeModalActions';
import { IEntityAction } from '../types/typesActions';
import { IApplicationState } from '../types/typesAppStore'
import { IDashboardDispatchProps, IDashboardProps, IDashboardStateProps } from '../types/typesDashboard'
import { IEmployee } from '../types/typesEntities'
import EmployeeListItem from './EmployeeListItem';

export class EmployeesDashboard extends React.Component<IDashboardProps<IEmployee>, {}> {

    componentDidMount() {
        this.props.fetchData();
    }

    render() {
        return (
            <div>
                <h1>Employees dashboard.</h1>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Company title</th>
                            <th scope="col">Department title</th>
                            <th scope="col">Full Name</th>
                            <th scope="col">Address</th>
                            <th scope="col">Position</th>
                            <th scope="col">Actions</th>
                        </tr>
                        <tr>
                            <th>
                                <button
                                    type="button"
                                    className="btn btn-lg btn-success"
                                    onClick={this.props.openCreateModal}
                                >+
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item: IEmployee, index: number)=> (
                            <EmployeeListItem
                                id={item.id}
                                key={item.id}
                                order={index}
                                companyId={item.companyId}
                                departmentId={item.departmentId}
                                fullName={item.fullName}
                                address={item.address}
                                position={item.position}
                            />
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps =
(
    state: IApplicationState,
    props: IDashboardProps<IEmployee>
) => ({
    data: state.employees.filter((employee) =>
        (employee.companyId === props.match.params.companyId) &&
        (employee.departmentId === props.match.params.departmentId))
});

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<IEmployee[], undefined, IEntityAction<IEmployee>>,
    props: IDashboardProps<IEmployee>
) => ({
    openCreateModal: () => dispatch(openEmployeeModalForCreate(props.match.params.companyId, props.match.params.departmentId)),
    fetchData: () => dispatch(asyncReadEmployees(props.match.params.companyId, props.match.params.departmentId))
});

const connectedEmployeesDashboard =
connect<IDashboardStateProps<IEmployee>, IDashboardDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(EmployeesDashboard);

export default connectedEmployeesDashboard;
