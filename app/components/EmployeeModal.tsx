import * as React from 'react';
import * as Modal from 'react-modal';
import { connect } from 'react-redux';

import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import {closeEmployeeModal} from '../store/actions/employeeModalActions';
import {customStyles} from '../types/defaults';
import {IApplicationState} from '../types/typesAppStore';
import {IEmployee} from '../types/typesEntities';
import {IModalDispatchProps, IModalProps, IModalState} from '../types/typesModal';
import EmployeeForm from './EmployeeForm';

export const EmployeeModal: React.SFC<IModalProps<IEmployee>> = (props) => (
    <div>
        <Modal
            isOpen={props.isModalOpen}
            onRequestClose={props.closeModal}
            style={customStyles}
            contentLabel="Employee Modal"
        >
            <h3></h3>
            <EmployeeForm
                id={props.entity.id}
                companyId={props.entity.companyId}
                departmentId={props.entity.departmentId}
                fullName={props.entity.fullName}
                address={props.entity.address}
                position={props.entity.position}
                isNew={props.isNew}
            />
        </Modal>
    </div>
);

const mapStateToProps = (state: IApplicationState) => state.employeeModal;
const mapDispatchToProps = (dispatch: ThunkDispatch<IModalState<IEmployee>, undefined, Action>) => ({
    closeModal: () => dispatch(closeEmployeeModal())
});

const connectedCompanyModal =
connect<IModalState<IEmployee>, IModalDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeModal);

export default connectedCompanyModal;
