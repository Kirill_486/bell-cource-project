import * as React from 'react';
import { connect } from 'react-redux';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';

import {createEmployee, updateEmployee} from '../store/actions/employeeActions';
import {closeEmployeeModal} from '../store/actions/employeeModalActions';
import {IEntityAction} from '../types/typesActions';
import {IEmployee} from '../types/typesEntities';
import { IEmployeeFormProps, IFormDispatchProps } from '../types/typesForm';

interface IEmployeeFormState extends IEmployee {
    errorMessage: string;
}

export class EmployeeForm extends React.Component<IEmployeeFormProps, IEmployeeFormState> {
    constructor(props: IEmployeeFormProps) {
        super(props);

        this.state = {
            id: props.id,
            companyId: props.companyId,
            departmentId: props.departmentId,
            fullName: props.fullName,
            address: props.address,
            position: props.position,
            errorMessage: ''
        }
    }

    onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    }

    onFullNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const fullName = e.target.value;
        this.setState({
            fullName
        });
    }

    onAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const address = e.target.value;
        this.setState({
            address
        });
    }

    onPositionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const position = e.target.value;
        this.setState({
            position
        });
    }

    render() {
       return (
           <form onSubmit={this.onSubmit}>
            <fieldset className="form-group">
                <label htmlFor="fullName">FullName</label>
                <input
                    onChange={this.onFullNameChange}
                    name="fullName"
                    type="text"
                    value={this.state.fullName}
                    className="form-control"
                />

                <label htmlFor="address">address</label>
                <input
                    onChange={this.onAddressChange}
                    name="address"
                    type="text"
                    value={this.state.address}
                    className="form-control"
                />
                <label htmlFor="position">address</label>
                <input
                    onChange={this.onPositionChange}
                    name="position"
                    type="text"
                    value={this.state.position}
                    className="form-control"
                />
            </fieldset>
            <div className="form-group">
                <input
                    type="submit"
                    value="Save changes"
                    className="form-control"
                    onClick={()=> {
                        console.log('click');
                        this.props.saveEntity(this.state)
                        this.props.closeModal();
                    }}
                />
                <input
                    type="submit"
                    value="Cancel"
                    className="form-control"
                    onClick={this.props.closeModal}
                />
            </div>
           </form>
       );
    }
}

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<IEmployee[], undefined, IEntityAction<IEmployee> | Action>,
    props: IEmployeeFormProps
) => ({
    saveEntity: (entity: IEmployee) => {
        if (props.isNew) {
            dispatch(createEmployee(entity))
        } else {
            dispatch(updateEmployee(entity))
        }
    },
    closeModal: () => dispatch(closeEmployeeModal())
})

const connectedEmployeeForm = connect<undefined, IFormDispatchProps<IEmployee>>
(
    undefined,
    mapDispatchToProps
)(EmployeeForm);

export default connectedEmployeeForm;
