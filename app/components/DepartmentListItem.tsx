import * as React from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk'

import {history} from '../routes/router';
import {deleteDepartment} from '../store/actions/departmentActions';
import {openDepartmentModalForEdit} from '../store/actions/departmentModalActions';
import {getCompanyTitleById} from '../store/selectors';
import { IEntityAction } from '../types/typesActions';
import {IApplicationState} from '../types/typesAppStore';
import { IDepartment } from '../types/typesEntities';
import {IDepartmentListItemProps, IDepartmentStateProps, IListItemDispatchProps} from '../types/typesListItem';

const DepartmentListItem: React.SFC<IDepartmentListItemProps> = (props) =>
(
<tr>
    <th scope="row">{props.order}</th>
    <td>{props.title}</td>
    <td>{props.companyTitle}</td>
    <td>{props.phone}</td>
    <td
        className="btn-group"
        role="group"
        aria-label="Basic example"
    >
        <button
            type="button"
            className="btn btn-primary btn-sm"
            onClick={()=> history.push(`/employee/${props.companyId}/${props.id}`)}
        >
            ->>
        </button>
        <button
            type="button"
            className="btn btn-warning btn-sm"
            onClick={()=> props.onEditButtonClick()}
        >
            edit
        </button>
        <button
            type="button"
            className="btn btn-danger btn-sm"
            onClick={props.onDeleteButtonClicked}
        >
            del
        </button>
    </td>
</tr>
);

const mapDispatchToProps = (
    dispatch: ThunkDispatch<IDepartment[], undefined, IEntityAction<IDepartment>>,
    props: IDepartmentListItemProps) =>
    (
        {
            onEditButtonClick: () => dispatch(openDepartmentModalForEdit(props)),
            onDeleteButtonClicked: () => dispatch(deleteDepartment(props.id))
    }
);

const mapStateToProps = (state: IApplicationState, props: IDepartmentListItemProps) => {
    state;
    const companyTitle = (props.companyId && getCompanyTitleById(props.companyId)) || 'not found';
    return {
        companyTitle
    }
}

const connectedDepartmentListItem = connect<IDepartmentStateProps, IListItemDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(DepartmentListItem);

export default connectedDepartmentListItem;
