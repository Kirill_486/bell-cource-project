import * as React from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk'

import {history} from '../routes/router'
import {deleteCompany} from '../store/actions/companyActions';
import {openCompanyModalForEdit} from '../store/actions/companyModalActions';
import { IEntityAction } from '../types/typesActions';
import { ICompany } from '../types/typesEntities';
import {ICompanyListItemProps, IListItemDispatchProps} from '../types/typesListItem';

export const CompanyListItem: React.SFC<ICompanyListItemProps> = (props) =>
(
<tr>
    <th scope="row">{props.order}</th>
    <td>{props.title}</td>
    <td>{props.address}</td>
    <td>{props.inn}</td>
    <td
        className="btn-group"
        role="group"
        aria-label="Basic example"
    >
        <button
            type="button"
            className="btn btn-primary btn-sm"
            onClick={()=> history.push(`/department/${props.id}`)}
        >
            ->>
        </button>
        <button
            type="button"
            className="btn btn-warning btn-sm"
            onClick={()=> props.onEditButtonClick()}
        >
            edit
        </button>
        <button
            type="button"
            className="btn btn-danger btn-sm"
            onClick={props.onDeleteButtonClicked}
        >
            del
        </button>
    </td>
</tr>
);

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<ICompany[], undefined, IEntityAction<ICompany>>,
    props: ICompanyListItemProps
) => ({
    onEditButtonClick: () => dispatch(openCompanyModalForEdit(props)),
    onDeleteButtonClicked: () => dispatch(deleteCompany(props.id))
});

const connectedCompanyListItem = connect<undefined, IListItemDispatchProps>
(
    undefined,
    mapDispatchToProps
)(CompanyListItem);

export default connectedCompanyListItem;
