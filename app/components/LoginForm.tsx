import * as React from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';
import {IApplicationState, IAuthState} from '../types/typesAppStore';
import {ICredentials} from '../types/typesEntities';

import { Action } from 'redux';
import { asyncLogin, ILoginAction, logout } from '../store/actions/auth'

interface ILoginStateProps {
  loginStatus?: boolean;
  loading?: boolean;
}

interface ILoginDispatchProps {
  login?: (credentials: ICredentials) => void;
  logout?: () => void;
}

interface ILoginProps extends ILoginStateProps, ILoginDispatchProps {

}

interface ILoginState {
    email: string,
    password: string
}

class LoginForm extends React.Component<ILoginProps, ILoginState> {

  constructor(props: ILoginProps) {
    super(props);

    this.state = {
      email: '',
      password: ''
    }
  }

  handleLogin = () => {
    const credentials: ICredentials = {
      email: this.state.email,
      password: this.state.password
    }

    this.props.login(credentials)
  };

  handleLogout = () => {
    this.props.logout();
  };

  onUserNameChanged = (e: React.FormEvent<HTMLInputElement>) => {
    const email = e.currentTarget.value;
    this.setState({
        email
    });
  }

  onPasswordChanged = (e: React.FormEvent<HTMLInputElement>) => {
    const password = e.currentTarget.value;
    this.setState({
        password
    });
  }

  render () {
    const {loginStatus, loading} = this.props;
    return (
      <form>
        <h3>
          Organiations App
        </h3>
        {
          loading ?
            <p>Please wait...</p> :
            loginStatus ?
              <p>
                Login success
              </p> :
              <p>
                Logged out
              </p>
        }

        {
          !loginStatus ?
          <div>
            <fieldset className="form-group">
              <input
                className="form-control"
                disabled={loading}
                type="email"
                name="user"
                placeholder="e-mail"
                onChange={this.onUserNameChanged}
              />
              <input
                className="form-control"
                disabled={loading}
                type="password"
                name="pass"
                placeholder="Password"
                onChange={this.onPasswordChanged}
              />
            </fieldset>
            <div className="form-group">
              <input
              className="btn btn-outline-primary form-control"
              disabled={loading}
              type="button"
              value="login"
              onClick={this.handleLogin}
              />
            </div>
          </div>
          :
          <div className="form-group">
            <input
              className="btn btn-outline-warning form-control"
              disabled={loading}
              type="button"
              value="logout"
              onClick={this.handleLogout}
            />
          </div>
        }

      </form>
    );
  }
};

function mapStateToProps(state: IApplicationState) {
  return {
    loginStatus: state.auth.loginStatus,
    loading: state.auth.loading,
  };
}

function mapDispatchToProps(dispatch: ThunkDispatch<IAuthState, undefined, Action | ILoginAction>) {
  return {
    login: (credentials: ICredentials) => dispatch(asyncLogin(credentials)),
    logout: () => dispatch(logout())
  };
}

const connectLoginPage = connect<ILoginStateProps, ILoginDispatchProps>
(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);

export default connectLoginPage;
