import * as React from 'react';

const ContactMePage: React.SFC<{}> = () => (
    <div>
        <h2>This project is authored by <strong>Kirill Penkin</strong></h2>
        <h3>
            <a
                href="https://kirill486.github.io/cv.front_end_developer/"
                >
                CV
            </a>
        </h3>
        <h3>
            <a
                href="https://bitbucket.org/Kirill_486/bell-cource-project/src"
                >
                Source
            </a>
        </h3>
    </div>
);

export default ContactMePage;
