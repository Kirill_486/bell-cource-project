import * as React from 'react';
import { connect } from 'react-redux';

import { Nav, Navbar, NavItem } from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import { history } from '../routes/router';

import {IApplicationState} from '../types/typesAppStore';

interface INavigationProps {
  isLoggedIn: boolean;
};

const navigateToDashboard = () => history.push('/companies');
const navigateToLogin = () => history.push('/');
const navigateToContact = () => history.push('/contact');

const Navigation = (props: INavigationProps) => (
    <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to={'/'}>
            Organiations App
          </NavLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight>
          <NavItem
            href="#"
            onClick={navigateToDashboard}
            disabled={!props.isLoggedIn}
          >
            Dashboard
          </NavItem>

          <NavItem
            href="#"
            onClick={navigateToLogin}
          >
            Login
          </NavItem>
          <NavItem
            href="#"
            onClick={navigateToContact}
          >
            Contact Me
          </NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
);

const mapStateToProps = (state: IApplicationState)=> ({
    isLoggedIn: state.auth.userName ? true : false
});

const connectedNavigation = connect(mapStateToProps)(Navigation);

export default connectedNavigation;
