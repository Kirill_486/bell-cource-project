import * as React from 'react';
import * as Modal from 'react-modal';
import { connect } from 'react-redux';

import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import {closeCompanyModal} from '../store/actions/companyModalActions';
import {customStyles} from '../types/defaults';
import {IApplicationState} from '../types/typesAppStore';
import {ICompany} from '../types/typesEntities';
import {IModalDispatchProps, IModalProps, IModalState} from '../types/typesModal';
import CompanyForm from './CompanyForm';

export const CompanyModal: React.SFC<IModalProps<ICompany>> = (props: IModalProps<ICompany>) => (
    <div>
        <Modal
            isOpen={props.isModalOpen}
            onRequestClose={props.closeModal}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <CompanyForm
                id={props.entity.id}
                title={props.entity.title}
                address={props.entity.address}
                inn={props.entity.inn}
                isNew={props.isNew}
            />
        </Modal>
    </div>
);

const mapStateToProps = (state: IApplicationState) => state.companyModal;
const mapDispatchToProps = (dispatch: ThunkDispatch<IModalState<ICompany>, undefined, Action>) => ({
    closeModal: () => dispatch(closeCompanyModal())
});

const connectedCompanyModal = connect<IModalState<ICompany>, IModalDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(CompanyModal);

export default connectedCompanyModal;
