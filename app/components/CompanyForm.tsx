import * as React from 'react';
import { connect } from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';

import { Action } from 'redux';
import {createCompany, updateCompany} from '../store/actions/companyActions';
import {closeCompanyModal} from '../store/actions/companyModalActions';
import {IEntityAction} from '../types/typesActions';
import {ICompany} from '../types/typesEntities';
import {ICompanyFormProps, ICompanyFormState, IFormDispatchProps} from '../types/typesForm';

export class CompanyForm extends React.Component<ICompanyFormProps, ICompanyFormState> {
    constructor(props: ICompanyFormProps) {
        super(props);

        this.state = {
            id: props.id,
            title: props.title,
            address: props.address,
            inn: props.inn,
            errorMessage: ''
        }
    }

    onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.saveEntity(this.state as ICompany);
        this.props.closeModal();
    }

    onTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const title = e.target.value;
        this.setState({
            title
        });
    }

    onAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const address = e.target.value;
        this.setState({
            address
        });
    }

    onInnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inn = e.target.value;
        this.setState({
            inn
        });
    }

    render() {
       return (
           <form onSubmit={this.onSubmit}>
            <fieldset className="form-group">
                <label htmlFor="title">Title</label>
                <input
                    onChange={this.onTitleChange}
                    name="title"
                    type="text"
                    value={this.state.title}
                    className="form-control"
                />

                <label htmlFor="address">Address</label>
                <input
                    onChange={this.onAddressChange}
                    name="address"
                    type="text"
                    value={this.state.address}
                    className="form-control"
                />

                <label htmlFor="inn">Inn</label>
                <input
                    onChange={this.onInnChange}
                    name="inn"
                    type="text"
                    value={this.state.inn}
                    className="form-control"
                />
            </fieldset>
            <div className="form-group">
                <input
                    type="submit"
                    value="Save changes"
                    className="form-control"
                    onClick={()=> {
                        console.log('click');
                    }}
                />
                <input
                    type="button"
                    value="Cancel"
                    className="form-control"
                    onClick={this.props.closeModal}
                />
            </div>
           </form>
       );
    }
}

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<ICompany[], undefined, IEntityAction<ICompany> | Action>,
    props: ICompanyFormProps
) => ({
    saveEntity: (entity: ICompany) => {
        if (props.isNew) {
            dispatch(createCompany(entity))
        } else {
            dispatch(updateCompany(entity))
        }
    },
    closeModal: () => dispatch(closeCompanyModal())
})

const connectedCompanyForm =
connect<undefined, IFormDispatchProps<ICompany>>
(
    undefined,
    mapDispatchToProps
)(CompanyForm);

export default connectedCompanyForm;
