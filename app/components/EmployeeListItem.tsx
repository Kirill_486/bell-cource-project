import * as React from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk'

import {deleteEmployee} from '../store/actions/employeeActions';
import {openEmployeeModalForEdit} from '../store/actions/employeeModalActions';
import {getCompanyTitleById, getDepartmentTitleById} from '../store/selectors';
import { IEntityAction } from '../types/typesActions';
import {IApplicationState} from '../types/typesAppStore';
import { IEmployee } from '../types/typesEntities';
import {IEmployeeListItemProps, IEmployeeStateProps, IListItemDispatchProps} from '../types/typesListItem';

export const EmployeeListItem: React.SFC<IEmployeeListItemProps> = (props) =>
(
<tr>
    <th scope="row">{props.order}</th>
    <td>{props.companyTitle}</td>
    <td>{props.departmentTitle}</td>
    <td>{props.fullName}</td>
    <td>{props.address}</td>
    <td>{props.position}</td>
    <td
        className="btn-group"
        role="group"
        aria-label="Basic example"
    >
        <button
            type="button"
            className="btn btn-warning btn-sm"
            onClick={()=> props.onEditButtonClick()}
        >
            edit
        </button>
        <button
            type="button"
            className="btn btn-danger btn-sm"
            onClick={props.onDeleteButtonClicked}
        >
            del
        </button>
    </td>
</tr>
);

const mapDispatchToProps = (dispatch: ThunkDispatch<IEmployee, undefined, IEntityAction<IEmployee>>, props: IEmployeeListItemProps) => ({
    onEditButtonClick: () => dispatch(openEmployeeModalForEdit(props)),
    onDeleteButtonClicked: () => dispatch(deleteEmployee(props.id))
});

const mapStateToProps = (state: IApplicationState, props: IEmployeeListItemProps) => {
    state;
    const companyTitle = (props.companyId && getCompanyTitleById(props.companyId)) || 'not found';
    const departmentTitle = (props.departmentId && getDepartmentTitleById(props.departmentId)) || 'not found';
    return {
        companyTitle,
        departmentTitle
    }
}

const connectedDepartmentListItem =
connect<IEmployeeStateProps, IListItemDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(EmployeeListItem);

export default connectedDepartmentListItem;
