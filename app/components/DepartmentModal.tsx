import * as React from 'react';
import * as Modal from 'react-modal';
import { connect } from 'react-redux';

import { Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import {closeDepartmentModal} from '../store/actions/departmentModalActions';
import {customStyles} from '../types/defaults';
import {IApplicationState} from '../types/typesAppStore';
import {IDepartment} from '../types/typesEntities';
import {IModalDispatchProps, IModalProps, IModalState} from '../types/typesModal';
import DepartmentForm from './DepartmentForm';

export const DepartmentModal: React.SFC<IModalProps<IDepartment>> = (props: IModalProps<IDepartment>) => (
    <div>
        <Modal
            isOpen={props.isModalOpen}
            onRequestClose={props.closeModal}
            style={customStyles}
            contentLabel="Department Modal"
        >
            <DepartmentForm
                id={props.entity.id}
                companyId={props.entity.companyId}
                title={props.entity.title}
                phone={props.entity.phone}
                isNew={props.isNew}
            />
        </Modal>
    </div>
);

const mapStateToProps = (state: IApplicationState) => state.departmentModal;
const mapDispatchToProps = (dispatch: ThunkDispatch<IModalState<IDepartment>, undefined, Action>) => ({
    closeModal: ()=> dispatch(closeDepartmentModal())
});

const connectedCompanyModal =
connect<IModalState<IDepartment>, IModalDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(DepartmentModal);

export default connectedCompanyModal;
