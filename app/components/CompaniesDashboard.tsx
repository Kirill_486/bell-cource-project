import * as React from 'react';
import { connect } from 'react-redux';

import { ThunkDispatch } from 'redux-thunk';
import {asyncReadCompanies} from '../store/actions/companyActions';
import {openCompanyModalForCreate} from '../store/actions/companyModalActions';
import { IEntityAction } from '../types/typesActions';
import {IApplicationState} from '../types/typesAppStore';
import {IDashboardDispatchProps, IDashboardProps, IDashboardStateProps} from '../types/typesDashboard';
import {ICompany} from '../types/typesEntities';
import CompanyListItem from './CompanyListItem';

export class CompaniesDashboard extends React.Component<IDashboardProps<ICompany>, {}> {

    componentDidMount() {
        this.props.fetchData();
    }

    render() {
        return (
            <div>
                <h1>Companies dashboard.</h1>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">title</th>
                            <th scope="col">address</th>
                            <th scope="col">inn</th>
                            <th scope="col">actions</th>
                        </tr>
                        <tr>
                            <th>
                                <button
                                    type="button"
                                    className="btn btn-lg btn-success"
                                    onClick={this.props.openCreateModal}
                                >+
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item: ICompany, index: number)=> (
                            <CompanyListItem
                                id={item.id}
                                key={item.id}
                                order={index}
                                title={item.title}
                                address={item.address}
                                inn={item.inn}
                            />
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state: IApplicationState) => ({
    data: state.companies
});

const mapDispatchToProps =
    (dispatch: ThunkDispatch<ICompany[], undefined, IEntityAction<ICompany>>
) => ({
    openCreateModal: () => dispatch(openCompanyModalForCreate()),
    fetchData: () => dispatch(asyncReadCompanies())
});

const connectedCompaniesDashboard =
connect<IDashboardStateProps<ICompany>, IDashboardDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(CompaniesDashboard);

export default connectedCompaniesDashboard;
