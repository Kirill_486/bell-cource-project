import * as React from 'react';
import LoginForm from './LoginForm';

const LoginPage: React.SFC<{}> = () => (
    <div>
        <LoginForm />
    </div>
);

export default LoginPage;
