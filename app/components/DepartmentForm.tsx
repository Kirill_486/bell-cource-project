import * as React from 'react';
import { connect } from 'react-redux';
import {Action} from 'redux'
import {ThunkDispatch} from 'redux-thunk';

import {createDepartment, updateDepartment} from '../store/actions/departmentActions';
import { closeDepartmentModal } from '../store/actions/departmentModalActions';
import {IEntityAction} from '../types/typesActions';
import {IDepartment} from '../types/typesEntities';
import {IDepartmentFormProps, IDepartmentFormState, IFormDispatchProps} from '../types/typesForm';

export class DepartmentForm extends React.Component<IDepartmentFormProps, IDepartmentFormState> {
    constructor(props: IDepartmentFormProps) {
        super(props);

        this.state = {
            id: props.id,
            companyId: props.companyId,
            title: props.title,
            phone: props.phone,
            errorMessage: ''
        }
    }

    onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    }

    onTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const title = e.target.value;
        this.setState({
            title
        });
    }

    onPhoneChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const phone = e.target.value;
        this.setState({
            phone
        });
    }

    render() {
       return (
           <form onSubmit={this.onSubmit}>
            <fieldset className="form-group">
                <label htmlFor="title">Title</label>
                <input
                    onChange={this.onTitleChange}
                    name="title"
                    type="text"
                    value={this.state.title}
                    className="form-control"
                />

                <label htmlFor="phone">phone</label>
                <input
                    onChange={this.onPhoneChange}
                    name="phone"
                    type="tel"
                    value={this.state.phone}
                    className="form-control"
                />
            </fieldset>
            <div className="form-group">
                <input
                    type="submit"
                    value="Save changes"
                    className="form-control"
                    onClick={()=> {
                        console.log('click');
                        this.props.saveEntity(this.state)
                        this.props.closeModal();
                    }}
                />
                <input
                    type="submit"
                    value="Cancel"
                    className="form-control"
                    onClick={this.props.closeModal}
                />
            </div>
           </form>
       );
    }
}

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<IDepartment[], undefined, IEntityAction<IDepartment> | Action>,
    props: IDepartmentFormProps
): IFormDispatchProps<IDepartment> => ({
    saveEntity: (entity) => {
        if (props.isNew) {
            dispatch(createDepartment(entity))
        } else {
            dispatch(updateDepartment(entity))
        }
    },
    closeModal: () => dispatch(closeDepartmentModal())
})

const connectedCompanyForm = connect<undefined, IFormDispatchProps<IDepartment>>
(
    undefined,
    mapDispatchToProps
)(DepartmentForm);

export default connectedCompanyForm;
