import * as React from 'react';
import { connect } from 'react-redux';

import {Col, Grid, Row} from 'react-bootstrap';
import { ThunkDispatch } from 'redux-thunk';
import {history} from '../routes/router';
import {asyncReadDepartments} from '../store/actions/departmentActions';
import {openDepartmentModalForCreate} from '../store/actions/departmentModalActions';
import { IEntityAction } from '../types/typesActions';
import {IApplicationState} from '../types/typesAppStore';
import {IDashboardDispatchProps, IDashboardProps, IDashboardStateProps} from '../types/typesDashboard';
import { IDepartment } from '../types/typesEntities';
import DepartmentListItem from './DepartmentListItem';

const backToCompaniesDashboard = () => history.push('/companies');

export class DepartmentsDashboard extends React.Component<IDashboardProps<IDepartment>, {}> {

    componentDidMount() {
        this.props.fetchData();
    }

    render() {
        return (
            <div>
                <Grid>
                    <Row>
                        <Col xs={4} md={2}>
                            <button
                                type="button"
                                className="btn btn-lg btn-warning"
                                onClick={backToCompaniesDashboard}
                            >Back
                            </button>
                        </Col>
                        <Col xs={8} md={10}>
                            <h1>Departments dashboard.</h1>
                        </Col>
                    </Row>
                </Grid>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Company title</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Actions</th>
                        </tr>
                        <tr>
                            <th>
                                <button
                                    type="button"
                                    className="btn btn-lg btn-success"
                                    onClick={this.props.openCreateModal}
                                >+
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item: IDepartment, index: number)=> (
                            <DepartmentListItem
                                id={item.id}
                                key={item.id}
                                order={index}
                                companyId={item.companyId}
                                title={item.title}
                                phone={item.phone}
                            />
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state: IApplicationState, props: IDashboardProps<IDepartment>) => ({
    data: state.departments.filter((department: IDepartment) => department.companyId === props.match.params.companyId)
});

const mapDispatchToProps =
(
    dispatch: ThunkDispatch<IDepartment[], undefined, IEntityAction<IDepartment>>,
    props: IDashboardProps<IDepartment>
) => ({
    openCreateModal: () => dispatch(openDepartmentModalForCreate(props.match.params.companyId)),
    fetchData: () => dispatch(asyncReadDepartments(props.match.params.companyId))
});

const connectedCompaniesDashboard =
connect<IDashboardStateProps<IDepartment>, IDashboardDispatchProps>
(
    mapStateToProps,
    mapDispatchToProps
)(DepartmentsDashboard);

export default connectedCompaniesDashboard;
