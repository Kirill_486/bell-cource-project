import createHistory from 'history/createBrowserHistory';
import * as React from 'react';
import { Route, Router, Switch } from 'react-router-dom';

import CompaniesDashboard from '../components/CompaniesDashboard';
import CompanyModal from '../components/CompanyModal';
import ContactMePage from '../components/ContactMePage';
import DepartmentsDashboard from '../components/DepartmentsDashboard';
import DepartmentModal from '../components/DepartmentModal';
import EmployeesDashboard from '../components/EmployeesDashboard';
import EmployeeModal from '../components/EmployeeModal';
import LoginPage from '../components/LoginPage';
import Navigaion from '../components/Navigation';
import NotFoundPage from '../components/NotFoundPage';
import PrivateRoute from './PrivateRoute';

export const history = createHistory();

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Navigaion />
      <div className="container">
        <CompanyModal />
        <DepartmentModal />
        <EmployeeModal />
        <div className="row">
          <div className="col-lg-12 text-center">
            <Switch>
              <Route path="/" component={LoginPage} exact={true} />
              <PrivateRoute path="/companies" component={CompaniesDashboard} />
              <PrivateRoute path="/department/:companyId" component={DepartmentsDashboard} />
              <PrivateRoute path="/employee/:companyId/:departmentId" component={EmployeesDashboard} />
              <Route path="/contact" component={ContactMePage}/>
              <Route component={NotFoundPage} />
            </Switch>
          </div>
        </div>
      </div>
    </div>

  </Router>
);

export default AppRouter;
