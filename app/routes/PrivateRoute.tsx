import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

import {IApplicationState} from '../types/typesAppStore'

interface IRouteProps {
  path?: string;
  isAuthenticated: boolean;
  component: any;
}

export const PrivateRoute: React.SFC<IRouteProps> = ({
  path,
  isAuthenticated,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props: any) => (
      isAuthenticated ? (
        <div>
          <Component {...props} />
        </div>
      ) : (
          <Redirect to="/" />
        )
    )} />
  );

const mapStateToProps = (state: IApplicationState) => ({
  isAuthenticated: state.auth.loginStatus
});

export default connect(mapStateToProps)(PrivateRoute);
