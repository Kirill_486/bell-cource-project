// import {ICredentials, users} from './constants';
import {users} from '../types/mockDefaults';
import {ICredentials} from '../types/typesEntities';

const urlSuccessfullyLogin = 'http://www.mocky.io/v2/5b55f8ce320000583c8280ce';
const urlFailLogin = 'http://www.mocky.io/v2/5b61672b300000f10d6a40d6';

const urlFetchCompanies = 'http://www.mocky.io/v2/5b67c58b330000550032d907';

const urlFetchDepartments100 = 'http://www.mocky.io/v2/5b6be7452f00007200893a66';
const urlFetchDepartments200 = 'http://www.mocky.io/v2/5b6be7692f00003700893a67';

const urlFetchEmployees100_1000 = 'http://www.mocky.io/v2/5b6c06922f00008500893b0c';
const urlFetchEmployees100_1001 = 'http://www.mocky.io/v2/5b6c06cc2f00003700893b0e';
const urlFetchEmployees200_2000 = 'http://www.mocky.io/v2/5b6c06e82f00003700893b11';
const urlFetchEmployees200_2001 = 'http://www.mocky.io/v2/5b6c07012f00007200893b13';

const urlFetchEmptyArray = 'http://www.mocky.io/v2/5b683bff3300005d0432db87';

export const getEmptyArrayUrl = () => urlFetchEmptyArray;

export const getLoginUrl = (credentials: ICredentials): string => {
    let url = urlFailLogin; //unauthenticated
    users.forEach((user) => {
        if ((user.email === credentials.email) && (user.password === credentials.password)) {
            url = urlSuccessfullyLogin; //authenticated
        }
    });
    return url;
}

export const getReadCompaniesUrl = () => urlFetchCompanies;

export const getReadDepartmentsUrl = (companyId: string) => {
    switch (companyId) {
        case '100': return urlFetchDepartments100;
        case '200': return urlFetchDepartments200;
        default: return urlFetchEmptyArray;
    }
}

export const getReadEmployeesUrl = (companyId: string, departmentId: string) => {
    switch (companyId) {
        case '100' :
            switch (departmentId) {
                case '1000': return urlFetchEmployees100_1000;
                case '1001': return urlFetchEmployees100_1001;
                default: return urlFetchEmptyArray;
            }
        case '200' :
            switch (departmentId) {
                case '2000': return urlFetchEmployees200_2000;
                case '2001': return urlFetchEmployees200_2001;
                default: return urlFetchEmptyArray;
            }
        default: return urlFetchEmptyArray;
    }
}
