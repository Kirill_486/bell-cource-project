import {Action, ActionCreator} from 'redux';

import {departmentDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {IDepartment} from '../../types/typesEntities';

export const openDepartmentModalForCreate: ActionCreator<IEntitySingleAction<IDepartment>> =
(companyId: string) => ({
    type: `${ModalActionType.OPENFORCREATE}${Entities.DEPARTMENT}`,
    payload: {
        ...departmentDefaults,
        companyId
    }
});

export const openDepartmentModalForEdit: ActionCreator<IEntitySingleAction<IDepartment>> =
(department: IDepartment) => ({
    type: `${ModalActionType.OPENFOREDIT}${Entities.DEPARTMENT}`,
    payload: department
});

export const closeDepartmentModal: ActionCreator<Action> =
() => ({
    type: `${ModalActionType.CLOSE}${Entities.DEPARTMENT}`
});
