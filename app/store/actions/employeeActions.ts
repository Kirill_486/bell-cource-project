const uuidv4 = require('uuid/v4');
import {Action, ActionCreator, Dispatch} from 'redux';
import {ThunkAction} from 'redux-thunk';

import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IActionType, IEntityAction, IEntityArrayAction, IEntitySingleAction} from '../../types/typesActions';
import {IEmployee} from '../../types/typesEntities';
import {getReadEmployeesUrl} from '../helpers';

export const createEmployee: ActionCreator<IEntitySingleAction<IEmployee>> =
    (employee: IEmployee) => ({
        type: `${CRUD.CREATE}${Entities.PERSON}`,
        payload: {
            ...employee,
            id: uuidv4()
        }
    });

export const updateEmployee: ActionCreator<IEntitySingleAction<IEmployee>> =
    (employee: IEmployee) => ({
        type: `${CRUD.UPDATE}${Entities.PERSON}`,
        payload: employee
    });

export const deleteEmployee: ActionCreator<IEntitySingleAction<IEmployee>> =
    (id: string) => {
        let payload: IEmployee = {
            id,
            departmentId: '',
            companyId: '',
            fullName: '',
            address: '',
            position: ''
        }
        return {
            type: `${CRUD.DELETE}${Entities.PERSON}`,
            payload
        }
    }

export const readEmployees: ActionCreator<IEntityArrayAction<IEmployee>> =
    (employees: IEmployee[]) => ({
        type: `${CRUD.READ}${Entities.PERSON}`,
        payload: employees
    });

const employeeBegin: ActionCreator<Action> = () => ({
    type: `${Entities.PERSON}${AsyncActionTypes.BEGIN}`
});

const employeeFail: ActionCreator<IActionType> = (payload: any) => ({
    type: `${Entities.PERSON}${AsyncActionTypes.FAILURE}`,
    payload
});

export const asyncReadEmployees: ActionCreator<ThunkAction<void, IEmployee[], undefined, IEntityAction<IEmployee>>> =
(companyId: string, departmentId: string) => {
    const action: ThunkAction<void, IEmployee[], undefined, IEntityAction<IEmployee>> =
    (dispatch: Dispatch<IEntityAction<IEmployee>>) => {
        dispatch(employeeBegin());

        const url = getReadEmployeesUrl(companyId, departmentId);

        fetch(url)
        .then((responce)=>{
            if (responce.ok) {
                return responce.json();
            } else {
                throw responce.statusText;
            }
        })
        .then((recievedData) => dispatch(readEmployees(recievedData)))
        .catch((errorStatus) => dispatch(employeeFail(errorStatus)))
    }

    return action;
};
