import { Action, ActionCreator } from 'redux';
import { ThunkDispatch } from 'redux-thunk'
import { ThunkAction } from 'redux-thunk';
import { ActionTypes, AsyncActionTypes } from '../../types/enums';
import { IAuthState } from '../../types/typesAppStore';
import { ICredentials } from '../../types/typesEntities';
import { getLoginUrl } from '../helpers';

export interface ILoginAction extends Action {
  payload: any;
}

export const loginBegin: ActionCreator<Action> = () => ({
  type:`${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`
});

export const loginSuccess: ActionCreator<ILoginAction> = (payload: any) => ({
    type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`,
    payload
  });

export const loginFailed: ActionCreator<ILoginAction> = (payload: any) => ({
  type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`,
  payload
});

export const logout: ActionCreator<Action> = () => ({
  type: `${ActionTypes.LOGOUT}`
});

export const asyncLogin: ActionCreator<ThunkAction<void, IAuthState, ICredentials, ILoginAction | Action>> =
(credentials: ICredentials) => {
  const action: ThunkAction<void, IAuthState, ICredentials, ILoginAction | Action> =
  (dispatch: ThunkDispatch<IAuthState, ICredentials, ILoginAction | Action>) => {
    dispatch(loginBegin());

    const options = {
      method: 'POST',
      body: JSON.stringify(credentials)
    }

    const url = getLoginUrl(credentials);

    fetch(url, options)
      .then((responce) => {
        if (responce.ok) {
          return responce.json();
        } else {
          throw responce.statusText;
        }
      })
      .then((recievedData) =>
        dispatch(loginSuccess(recievedData))
      )
      .catch((errorStatus) =>
        dispatch(loginFailed(errorStatus))
      )
  }
  return action;
}
