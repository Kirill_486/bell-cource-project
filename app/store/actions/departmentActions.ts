const uuidv4 = require('uuid/v4');
import {Action, ActionCreator, Dispatch} from 'redux';
import {ThunkAction} from 'redux-thunk';

import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IActionType, IEntityAction, IEntityArrayAction, IEntitySingleAction} from '../../types/typesActions';
import {IDepartment} from '../../types/typesEntities';
import {getReadDepartmentsUrl} from '../helpers';

export const createDepartment: ActionCreator<IEntitySingleAction<IDepartment>> =
    (department: IDepartment) => ({
        type: `${CRUD.CREATE}${Entities.DEPARTMENT}`,
        payload: {
            ...department,
            id: uuidv4()
        }
    });

export const updateDepartment: ActionCreator<IEntitySingleAction<IDepartment>> =
    (department: IDepartment) => ({
        type: `${CRUD.UPDATE}${Entities.DEPARTMENT}`,
        payload: department
    });

export const deleteDepartment: ActionCreator<IEntitySingleAction<IDepartment>> =
    (id: string) => {
        let payload: IDepartment = {
            id,
            companyId: '',
            phone: '',
            title: ''
        }
        return {
            type: `${CRUD.DELETE}${Entities.DEPARTMENT}`,
            payload
        }
    };

const departmentBegin: ActionCreator<Action> = () => ({
    type: `${Entities.DEPARTMENT}${AsyncActionTypes.BEGIN}`
});

const departmentFail: ActionCreator<IActionType> = (payload: any) => ({
    type: `${Entities.DEPARTMENT}${AsyncActionTypes.FAILURE}`,
    payload
});

export const readDepartments: ActionCreator<IEntityArrayAction<IDepartment>> =
    (departments: IDepartment[]) => ({
        type: `${CRUD.READ}${Entities.DEPARTMENT}`,
        payload: departments
    });

export const asyncReadDepartments: ActionCreator<ThunkAction<void, IDepartment[], undefined, IEntityAction<IDepartment>>> =
(companyId: string) => {
    const action: ThunkAction<void, IDepartment[], undefined, IEntityAction<IDepartment>> =
    (dispatch: Dispatch<IEntityAction<IDepartment>>) => {
        dispatch(departmentBegin());

        const url = getReadDepartmentsUrl(companyId);

        fetch(url)
        .then((responce) => {
            if (responce.ok) {
                return responce.json();
            } else {
                throw responce.statusText;
            }
        })
        .then((recievedData) => dispatch(readDepartments(recievedData)))
        .catch((errorStatus) => dispatch(departmentFail(errorStatus)));
    }
    return action;
}
