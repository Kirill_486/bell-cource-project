import {Action, ActionCreator} from 'redux';

import {employeeDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {IEmployee} from '../../types/typesEntities';

export const openEmployeeModalForCreate: ActionCreator<IEntitySingleAction<IEmployee>> =
(companyId: string, departmentId: string) => ({
    type: `${ModalActionType.OPENFORCREATE}${Entities.PERSON}`,
    payload: {
        ...employeeDefaults,
        companyId,
        departmentId
    }
});

export const openEmployeeModalForEdit: ActionCreator<IEntitySingleAction<IEmployee>> =
(employee: IEmployee) => ({
    type: `${ModalActionType.OPENFOREDIT}${Entities.PERSON}`,
    payload: employee
});

export const closeEmployeeModal: ActionCreator<Action> =
() => ({
    type: `${ModalActionType.CLOSE}${Entities.PERSON}`
});
