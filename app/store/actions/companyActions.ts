/// https://gist.github.com/milankorsos/ffb9d32755db0304545f92b11f0e4beb
import {Action, ActionCreator, Dispatch} from 'redux';
import {ThunkAction} from 'redux-thunk';

const uuidv4 = require('uuid/v4');

import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IActionType, IEntityAction, IEntityArrayAction, IEntitySingleAction} from '../../types/typesActions';
import {ICompany} from '../../types/typesEntities';
import {getReadCompaniesUrl} from '../helpers';

export const createCompany: ActionCreator<IEntitySingleAction<ICompany>> =
    (company: ICompany) => ({
        type: `${CRUD.CREATE}${Entities.COMPANY}`,
        payload: {
            ...company,
            id: uuidv4()
        }
    });

export const updateCompany: ActionCreator<IEntitySingleAction<ICompany>> =
    (company: ICompany) => ({
        type: `${CRUD.UPDATE}${Entities.COMPANY}`,
        payload: company
    });

export const deleteCompany: ActionCreator<IEntitySingleAction<ICompany>> =
    (id: string) => {
        let payload: ICompany = {
            id,
            address: '',
            inn: '',
            title: ''
        }
        return {
            type: `${CRUD.DELETE}${Entities.COMPANY}`,
            payload
        }
    }

export const readCompanies: ActionCreator<IEntityArrayAction<ICompany>> =
    (companies: ICompany[]) => ({
        type: `${CRUD.READ}${Entities.COMPANY}`,
        payload: companies
    });

const companyBegin: ActionCreator<Action> = () => ({
    type: `${Entities.COMPANY}${AsyncActionTypes.BEGIN}`
});

const companyFail: ActionCreator<IActionType> = (payload: any) => ({
    type: `${Entities.COMPANY}${AsyncActionTypes.FAILURE}`,
    payload
});

export const asyncReadCompanies:
    ActionCreator<ThunkAction<void, ICompany[], undefined, IEntityAction<ICompany>>> =
    () => {
        const action: ThunkAction<void, ICompany[], undefined, IEntityAction<ICompany>> =
        (dispatch: Dispatch<IEntityAction<ICompany>>) => {
            dispatch(companyBegin());

            const url = getReadCompaniesUrl();

            fetch(url)
            .then((responce)=>{
                if (responce.ok) {
                    return responce.json();
                } else {
                    throw responce.statusText;
                }
            })
            .then((recievedData) => dispatch(readCompanies(recievedData)))
            .catch((errorStatus) => dispatch(companyFail(errorStatus)))
        }

        return action;
}
