import {Action, ActionCreator} from 'redux';

import {companyDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {ICompany} from '../../types/typesEntities';

export const openCompanyModalForCreate: ActionCreator<IEntitySingleAction<ICompany>> =
() => ({
    type: `${ModalActionType.OPENFORCREATE}${Entities.COMPANY}`,
    payload: companyDefaults
});

export const openCompanyModalForEdit: ActionCreator<IEntitySingleAction<ICompany>> =
(company: ICompany) => ({
    type: `${ModalActionType.OPENFOREDIT}${Entities.COMPANY}`,
    payload: company
});

export const closeCompanyModal: ActionCreator<Action> = () => ({
    type: `${ModalActionType.CLOSE}${Entities.COMPANY}`
});
