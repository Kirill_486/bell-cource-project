import { combineReducers, createStore } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { applyMiddleware } from 'redux';
import authReducer from './reducers/auth';
import companyModalReducer from './reducers/companyModalReducer';
import companiesReducer from './reducers/companyReducer';
import departmentModalReducer from './reducers/departmentModalReducer';
import departmentReducer from './reducers/departmentReducer';
import employeeModalReducer from './reducers/employeeModalReducer';
import employeeReducer from './reducers/employeeReducer';

const store = createStore(combineReducers({
    companies: companiesReducer,
    departments: departmentReducer,
    employees: employeeReducer,
    auth: authReducer,
    companyModal: companyModalReducer,
    departmentModal: departmentModalReducer,
    employeeModal: employeeModalReducer
}), composeWithDevTools(applyMiddleware(thunk)));

export default store;
