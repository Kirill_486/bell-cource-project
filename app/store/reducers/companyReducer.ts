import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IEntityAction} from '../../types/typesActions';
import {ICompany} from '../../types/typesEntities';

let initialCompanies: ICompany[] = [];

const companiesReducer = (state: ICompany[] = initialCompanies, action: IEntityAction<ICompany> ) => {
    const isPayloadArray = Array.isArray(action.payload);

    switch (action.type) {

        case `${CRUD.CREATE}${Entities.COMPANY}` :
            return isPayloadArray ? state :
            [
                ...state,
                action.payload
            ];
        case `${CRUD.READ}${Entities.COMPANY}` :
            return isPayloadArray ? (action.payload as ICompany[]) : state ;
        case `${CRUD.UPDATE}${Entities.COMPANY}` :
            return isPayloadArray ? state : state.map((item)=> item.id !== (action.payload as ICompany).id ? item : (action.payload as ICompany));
        case `${CRUD.DELETE}${Entities.COMPANY}` :

            if (isPayloadArray){
                return state;
            } else {
                let payload = action.payload as ICompany;
                return state.filter((item)=>item.id !== payload.id);
            }
        case `${Entities.COMPANY}${AsyncActionTypes.FAILURE}`:
            console.error(action.payload);
            return initialCompanies;

        default:
            return state;
    }
}

export default companiesReducer;
