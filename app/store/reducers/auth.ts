import {ActionTypes, AsyncActionTypes} from '../../types/enums';
import {IActionType} from '../../types/typesActions';
import {IAuthState} from '../../types/typesAppStore';

const initialState = {
  loginStatus: false,
  loading: false,
  userName: ''
};

export default function authReducer (state: IAuthState = initialState, action: IActionType) {
    switch (action.type) {

    case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
    return {
      loading: true
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
    return {
      loginStatus: action.payload.data.authorized,
      loading: false,
      userName:action.payload.data.userName
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
    console.error(`Login failed. Reason: ${action.payload}`)
    return {
      loading: false,
      loginStatus: false,
      userName: ''
    };

    case ActionTypes.LOGOUT:
    return {
      loginStatus: false,
      userName: ''
    };
  }
  return state;
}
