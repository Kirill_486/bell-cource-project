import {departmentDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {IDepartment} from '../../types/typesEntities';
import {IModalState} from '../../types/typesModal';

const initialDepartmentModal: IModalState<IDepartment> = {
    entity: departmentDefaults,
    isModalOpen: false,
    isNew: true
}

const companyModalReducer = (
    state: IModalState<IDepartment> = initialDepartmentModal,
    action: IEntitySingleAction<IDepartment>
) => {
    switch (action.type) {
        case `${ModalActionType.OPENFORCREATE}${Entities.DEPARTMENT}`:
            return {
                entity: action.payload,
                isModalOpen: true,
                isNew: true
            }
        case `${ModalActionType.OPENFOREDIT}${Entities.DEPARTMENT}`:
            return {
                entity: action.payload,
                isModalOpen: true,
                isNew: false
            }
        case `${ModalActionType.CLOSE}${Entities.DEPARTMENT}`:
            return {
                entity: departmentDefaults,
                isModalOpen: false,
                isNew: true
            }
        default: return state;
    }
}

export default companyModalReducer;
