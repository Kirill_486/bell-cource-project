import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IEntityAction} from '../../types/typesActions';
import {IEmployee} from '../../types/typesEntities';

const initialEmployees: IEmployee[] = [];

const departmentsReducer = (state: IEmployee[] = initialEmployees, action: IEntityAction<IEmployee> ) => {
    const isPayloadArray = Array.isArray(action.payload);

    switch (action.type) {

        case `${CRUD.CREATE}${Entities.PERSON}` :
            return isPayloadArray ? state :
            [
                ...state,
                action.payload
            ];
        case `${CRUD.READ}${Entities.PERSON}` :
            return isPayloadArray ? (action.payload as IEmployee[]) : state ;
        case `${CRUD.UPDATE}${Entities.PERSON}` :
            return isPayloadArray ? state : state.map((item)=> item.id !== (action.payload as IEmployee).id ? item : (action.payload as IEmployee));
        case `${CRUD.DELETE}${Entities.PERSON}` :
            if (isPayloadArray){
                return state;
            } else {
                let payload = action.payload as IEmployee;
                return state.filter((item)=>item.id !== payload.id);
            }
        case `${Entities.PERSON}${AsyncActionTypes.FAILURE}`:
            console.error(action.payload);
            return initialEmployees;
        default:
            return state;
    }
}

export default departmentsReducer;
