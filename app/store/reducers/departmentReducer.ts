import {AsyncActionTypes, CRUD, Entities} from '../../types/enums';
import {IEntityAction} from '../../types/typesActions';
import {IDepartment} from '../../types/typesEntities';

const initialDepartments: IDepartment[] = [];

const departmentsReducer = (state: IDepartment[] = initialDepartments, action: IEntityAction<IDepartment> ) => {
    const isPayloadArray = Array.isArray(action.payload);

    switch (action.type) {

        case `${CRUD.CREATE}${Entities.DEPARTMENT}` :
            return isPayloadArray ? state :
            [
                ...state,
                action.payload
            ];
        case `${CRUD.READ}${Entities.DEPARTMENT}` :
            return isPayloadArray ? (action.payload as IDepartment[]) : state ;
        case `${CRUD.UPDATE}${Entities.DEPARTMENT}` :
            return isPayloadArray ? state : state.map((item)=> item.id !== (action.payload as IDepartment).id ? item : (action.payload as IDepartment));
        case `${CRUD.DELETE}${Entities.DEPARTMENT}` :
            if (isPayloadArray){
                return state;
            } else {
                let payload = action.payload as IDepartment;
                return state.filter((item)=>item.id !== payload.id);
            }
        case `${Entities.DEPARTMENT}${AsyncActionTypes.FAILURE}`:
            console.error(action.payload);
            return initialDepartments;
        default:
            return state;
    }
}

export default departmentsReducer;
