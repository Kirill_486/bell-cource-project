import {employeeDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {IEmployee} from '../../types/typesEntities';
import {IModalState} from '../../types/typesModal';

const initialEmployeeModal: IModalState<IEmployee> = {
    entity: employeeDefaults,
    isModalOpen: false,
    isNew: true
}

const employeeModalReducer = (
    state: IModalState<IEmployee> = initialEmployeeModal,
    action: IEntitySingleAction<IEmployee>
) => {
    switch (action.type) {
        case `${ModalActionType.OPENFORCREATE}${Entities.PERSON}`:
            return {
                entity: action.payload,
                isModalOpen: true,
                isNew: true
            }
        case `${ModalActionType.OPENFOREDIT}${Entities.PERSON}`:
            return {
                entity: action.payload,
                isModalOpen: true,
                isNew: false
            }
        case `${ModalActionType.CLOSE}${Entities.PERSON}`:
            return {
                entity: employeeDefaults,
                isModalOpen: false,
                isNew: true
            }
        default: return state;
    }
}

export default employeeModalReducer;
