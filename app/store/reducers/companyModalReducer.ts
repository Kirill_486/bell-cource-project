import {companyDefaults} from '../../types/defaults';
import {Entities, ModalActionType} from '../../types/enums';
import {IEntitySingleAction} from '../../types/typesActions';
import {ICompany} from '../../types/typesEntities';
import {IModalState} from '../../types/typesModal';

const initialCompanyModal: IModalState<ICompany> = {
    entity: companyDefaults,
    isModalOpen: false,
    isNew: true
}

const companyModalReducer = (
    state: IModalState<ICompany> = initialCompanyModal,
    action: IEntitySingleAction<ICompany>
) => {
    switch (action.type) {
        case `${ModalActionType.OPENFORCREATE}${Entities.COMPANY}`:
            return {
                entity: companyDefaults,
                isModalOpen: true,
                isNew: true
            }
        case `${ModalActionType.OPENFOREDIT}${Entities.COMPANY}`:
            return {
                entity: action.payload,
                isModalOpen: true,
                isNew: false
            }
        case `${ModalActionType.CLOSE}${Entities.COMPANY}`:
            return {
                entity: companyDefaults,
                isModalOpen: false,
                isNew: true
            }
        default: return state;
    }
}

export default companyModalReducer;
