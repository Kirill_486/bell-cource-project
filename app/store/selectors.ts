import {ICompany, IDepartment} from '../types/typesEntities';
import store from './configureStore';

const getCompanyById = (
        companies: ICompany[],
        companyId: string
    ) =>
    companies.find((company: ICompany) => company.id === companyId);

export const getCompanyTitleById = (companyId: string) => {
    const companies = store.getState().companies;
    const theCompany = getCompanyById(companies, companyId);
    return theCompany ? theCompany.title : 'not found';
};

const getDepartmentById = (departments: IDepartment[], departmentId: string) => departments.find((department: IDepartment) => department.id === departmentId);

export const getDepartmentTitleById = (departmentId: string) => {
    const departments = store.getState().departments;
    const theDepartment = getDepartmentById(departments, departmentId);
    return theDepartment ? theDepartment.title : 'not found';
};
